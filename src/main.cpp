#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <sstream>

#include "hoststatreader.h"
#include "configuration.h"
#include "logger.h"
#include "JSONBuilder.h"
#include "RestController.h"


Logger * logger; //Logger

void get_configuration();
void display_license();
void display_help();
void heartbeat();
void get_logging_level();

int main(int argc, char *argv[])
{
    Configuration * config = Configuration::getInstance();
    get_logging_level();

    if (argc > 1)
    {
        if ((strcmp(argv[1],"-l") == 0)|| (strcmp(argv[1],"--license") == 0)) { display_license(); return EXIT_SUCCESS; }
        else if ((strcmp(argv[1],"-h") == 0) || (strcmp(argv[1],"--help") == 0)) { display_help(); return EXIT_SUCCESS; }
    }

    logger->info("Starting proc!");
    while(true)
    {
        heartbeat();
        sleep(config->getInt("ITERATION_DELAY"));
    }
}

void heartbeat()
{
    Configuration * config = Configuration::getInstance();
    HostStatReader * reader = new HostStatReader();
    reader->setThermalZone(config->getString("THERMAL_ZONE_PATH"));
    reader->setNetStatsPath(config->getString("NET_STATS_PATH"));

    if(config->getBool("ADC_READING"))
        reader->setMCPDevSpiPath(config->getString("DEV_SPI_PATH"));

    StatContainer * cont = reader->getData();
    JSONBuilder * bui = new JSONBuilder(cont);
    RestController * rc = new RestController();
    rc->setBaseHostUrl(config->getString("BASE_REST_URL"));
    rc->setJSONBuilder(bui);
    rc->post(config->getString("POST_URI"));

    delete reader;
    delete bui;
    delete rc;
}

void get_logging_level()
{
    Configuration * config = Configuration::getInstance();
    std::string LOGGING = config->getString("LOGGING");
    logger = Logger::getInstance();

    std::transform(LOGGING.begin(), LOGGING.end(), LOGGING.begin(), ::toupper);

    if (LOGGING == "INFO")
        logger->setLoggingLevel(LogType::INFO);
    if (LOGGING == "DEBUG")
        logger->setLoggingLevel(LogType::DEBUG);
    if (LOGGING == "TRACE")
        logger->setLoggingLevel(LogType::TRACE);

}

void display_license()
{
    std::cout << "    proc\n"
                 "    Copyright (C) 2019    Evan R. Jaramillo,\n"
                 "                          M. Einaam Alim,\n"
                 "                          Evan C. Swinney\n"
                 "\n"
                 "    This program is free software: you can redistribute it and/or modify\n"
                 "    it under the terms of the GNU General Public License as published by\n"
                 "    the Free Software Foundation version 3.\n"
                 "\n"
                 "    This program is distributed in the hope that it will be useful,\n"
                 "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                 "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                 "    GNU General Public License for more details.\n"
                 "\n"
                 "    You should have received a copy of the GNU General Public License\n"
                 "    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n" << std::endl;
}

void display_help()
{
    std::cout << "Proc - Performance measurement gathering application\n\n"
                 "Usage: \n"
                 "[-h|--help]: Displays the help message.\n"
                 "[-l|--license]: Displays licencing information.\n" << std::endl;
}
