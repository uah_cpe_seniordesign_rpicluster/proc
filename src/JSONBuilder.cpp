//
// Created by evan on 12/24/18.
//

#include "JSONBuilder.h"

JSONBuilder::JSONBuilder(StatContainer *cont)
{
    this->container = cont;
    logger = Logger::getInstance();
}

JSONBuilder::~JSONBuilder()
{

}

json::value JSONBuilder::getJsonObject()
{
    json::value tempJson;

    try
    {
        tempJson["hostname"] = json::value::string(container->hostname);
        std::vector<std::vector<double>> usage = container->perCoreCpuPercentage;

        for (size_t i = 0; i < usage.size(); i++)
        {
            if (i == 0)
                tempJson["totalCpuUtilization"] = json::value::number(usage[i][1]);
            tempJson["perCoreCpuUtilization"][i] = json::value::number(usage[i][1]);
        }

    //    felt like it should have worked, but didn't because apparently putting a vector directly into a JSON object is just way too difficult for this Microsoft library
    //    std::vector<double> tempVec; //Used to concat the small little vectors into one big vector so I can easily put into json::value::array
    //    tempJson["totalCpuUtilization"] = json::value::number(usage[0][1]); //Total utilization for "Core" #0, item #1, which is total used
    //    for (std::vector<double> coreInfo: usage)
    //    {
    //        tempVec.push_back(coreInfo.at(1)); //Push all of the utilization numbers (at index 1) onto this temp vector
    //    }
    //    tempJson["perCoreCpuUtilization"] = json::value::array(tempVec);

        tempJson["physicalMemoryTotal"] = json::value(container->pMem);
        tempJson["physicalMemoryUsed"] = json::value(container->pMemUsed);
        tempJson["virtualMemoryTotal"] = json::value(container->vMem);
        tempJson["virtualMemoryUsed"] = json::value(container->vMemUsed);
        tempJson["rxBytes"] = json::value(container->rx_bytes);
        tempJson["txBytes"] = json::value(container->tx_bytes);
        tempJson["rxPackets"] = json::value(container->rx_packets);
        tempJson["txPackets"] = json::value(container->tx_packets);
        tempJson["cpuTemperature"] = json::value(container->temperature);
        tempJson["ambientTemp"] = json::value(container->ambientTemperature);
        tempJson["moduleCurrentDraw"] = json::value(container->moduleCurrentDraw);
        tempJson["backplaneVoltage"] = json::value(container->backplaneVoltage);
        tempJson["processorVoltage"] = json::value(container->processorVoltage);
    }
    catch(const std::exception &e)
    {
        logger->warning("Caught exception in getJsonObject: ", e.what());
    }
    return tempJson;
}
