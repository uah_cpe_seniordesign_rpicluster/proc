#include "mcpreader.h"

MCPReader::MCPReader(const std::string &devSpiPath)
{
    this->logger = Logger::getInstance();
    logger->debug("Constructing an MCPReader");
    this->devspiPath = devSpiPath;

    this->mcp = new MCP320X(devspiPath, MCP320X::Temperature, MCP320X::Current, MCP320X::Voltage, MCP320X::Voltage);
}

MCPReader::~MCPReader()
{
    logger->debug("Deconstructing an MCPReader");
    delete this->mcp;
}

void MCPReader::printInfo(const std::tuple<double, std::string> &info) const
{
    this->logger->trace("MCPReader info: (" + std::to_string(std::get<0>(info)) + ", " + std::get<1>(info) + ")");
}

void MCPReader::printInfo(const std::vector<std::tuple<double, std::string>> &info) const
{
    std::string temp = "MCPReader info: [";
    for(unsigned long i = 0; i < info.size(); i++)
    {
        temp += "(" + std::to_string(std::get<0>(info.at(i))) + "," + std::get<1>(info.at(i)) + ")";
        temp += (i != info.size()-1) ? "," : "";
    }
    temp += "]";
    this->logger->trace(temp);
}

std::tuple<double, std::string> MCPReader::getChannel(const int ch) const
{
    std::tuple<double, std::string> temp = this->mcp->getChannel(ch);

    switch(ch)
    {
        case 2: std::get<0>(temp) *= 2.0; break; //Channel 2 is a 2:1 voltage divider, so we need to fix the value from the raw voltage
        case 3: std::get<0>(temp) *= 6.0; break; //Channel 3 is a 6:1 voltage divider, so we need to fix the value from the raw voltage
        default: break;
    }

    printInfo(temp);

    return temp;
}

std::vector<std::tuple<double, std::string>> MCPReader::getAllChannels() const
{
    std::vector<std::tuple<double, std::string>> temp = this->mcp->getAllChannels();

    std::get<0>(temp.at(2)) *= 2.0; //Channel 2 is a 2:1 voltage divider, so we need to fix the value from the raw voltage
    std::get<0>(temp.at(3)) *= 6.0; //Channel 3 is a 6:1 voltage divider, so we need to fix the value from the raw voltage

    printInfo(temp);

    return temp;
}
