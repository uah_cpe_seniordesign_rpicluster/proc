//
// Created by evan on 1/23/19.
//

#include "MemoryStatReader.h"

MemoryStatReader::MemoryStatReader()
{
    this->logger = Logger::getInstance();
    sysinfo(&this->memInfo); //Populate the struct.
}
MemoryStatReader::~MemoryStatReader()
{
    logger->debug("Destruction of MemoryStatReader()");
}
int64_t MemoryStatReader::getTotalVMem()
{
    int64_t tVMem = memInfo.totalram;
    tVMem += memInfo.totalswap;
    tVMem *= memInfo.mem_unit;
    return tVMem;
}
int64_t MemoryStatReader::getTotalVMemUsed()
{
    int64_t tVMemU = memInfo.totalram - memInfo.freeram;
    tVMemU += memInfo.totalswap - memInfo.freeswap;
    tVMemU *= memInfo.mem_unit;
    return tVMemU;
}
int64_t MemoryStatReader::getTotalPMem()
{
    int64_t tPMem = memInfo.totalram;
    tPMem *= memInfo.mem_unit;
    return tPMem;
}
int64_t MemoryStatReader::getTotalPMemUsed()
{
    int64_t tPMemU = memInfo.totalram - memInfo.freeram;
    tPMemU *= memInfo.mem_unit;
    return tPMemU;
}