//
// Created by evan on 12/24/18.
//

#include "RestController.h"

RestController::RestController()
{
    this->init();
}
RestController::RestController(JSONBuilder * bui, const std::string &host)
{
    this->init();
    this->builder = bui;
    this->baseHost = host;
}
RestController::~RestController()
{
}
void RestController::init()
{
    logger = Logger::getInstance();
    this->baseHost = "";
    this->builder = nullptr;
}
bool RestController::status()
{
    if (this->builder == nullptr)
    {
        logger->warning("No JSON builder specified.");
        return false;
    }
    if (this->baseHost.empty())
    {
        logger->warning("No base host set.");
        return false;
    }
    return true;
}
void RestController::setBaseHostUrl(const std::string &baseHost)
{
    this->baseHost = baseHost;
}
void RestController::setJSONBuilder(JSONBuilder * bui)
{
    this->builder = bui;
}
void RestController::post(const std::string &uri)
{
    if (!this->status() || uri.empty()) //Check the object's status.
        return;

    try
    {
        web::json::value toPost = this->builder->getJsonObject();
        web::http::client::http_client client(this->baseHost);
        client.request(http::methods::POST, uri, toPost).wait();
        logger->debug("Sent data object to remote host.");
    }
    catch(const std::exception &e)
    {
        logger->warning("Caught exception in RestController: " + std::string(e.what()));
    }
}
