//
// Created by evan on 10/14/18.
//

#include "configuration.h"

Configuration * Configuration::instance = nullptr;
std::mutex Configuration::l_mutex;

Configuration::Configuration()
{
    logger = Logger::getInstance();
    logger->debug("Constructing configuration reader.");
    this->init();
    configFile.open("./dconfig.cnf",std::fstream::in);
    this->setDefault();
    this->readFile();
}

Configuration::~Configuration()
{

}

Configuration * Configuration::getInstance()
{
    std::lock_guard<std::mutex> lock(l_mutex);
    if (instance == nullptr)
        instance = new Configuration();

    return instance;
}

void Configuration::init()
{
    params = new std::map<std::string,std::string>();
    isDone = false;
}

void Configuration::setDefault()
{
    //Supply default values to the daemon so it doesnt crash!
    params->insert({"BASE_REST_URL", "http://127.0.0.1:8081"});
    params->insert({"POST_URI","/api/addHostData"});
    params->insert({"ITERATION_DELAY","5"});
    params->insert({"LOGGING","INFO"});
    params->insert({"THERMAL_ZONE_PATH","/sys/class/thermal/thermal_zone0/temp"}); //Default for raspberry pi
    params->insert({"NET_STATS_PATH","/sys/class/net/eth0/statistics"}); // " "
    params->insert({"ADC_READING","ENABLE"}); //By default, lets read the ADCs since all of these other defaults are for the Raspberry Pi
    params->insert({"DEV_SPI_PATH","/dev/spidev0.0"}); //Default for our project
}

void Configuration::readFile()
{
    if (!configFile.is_open())
        return; //Leave with default data

    logger->debug("reading configuration from file");
    std::string temp1;
    while (std::getline(configFile,temp1))
    {
        if (temp1.find('#') != -1) //If we find a no no character
            fileContents.push_back(temp1.substr(0,temp1.find('#'))); //Trim the string
        else //String contains no comments
        {
            fileContents.push_back(temp1); //Push it BAAACCCKKKK
        }
    }
    configFile.close(); //Glose the vile
    for (int i = 0; i < fileContents.size(); i++)
    {
        std::string temp = fileContents.at(i);
        temp.erase(remove_if(temp.begin(),temp.end(), isspace),temp.end()); //Remove all spaces
        std::istringstream ss(temp);
        std::vector<std::string> key_mapping = this->split(temp,'=');
        if (key_mapping.size() != 2)
            continue;

        //Key processing:
        std::string tempKey = key_mapping[0];
        std::transform(tempKey.begin(),tempKey.end(),tempKey.begin(), ::toupper); //Uppercase key.

        //Value processing:
        std::string tempVal = key_mapping[1];

        auto it = params->find(tempKey);
        if (it != params->end()) //If found in the map already.
        {
            it->second = tempVal; //update the value of the map (probably replacing the default values).
        }
        else //not found already. possible typo? im leaving this for debugging, but we should probably remove it.
        {
            logger->warning("No default value specified for key: ",tempKey,", consider adding default.");
            params->insert(std::pair<std::string,std::string>(tempKey,tempVal));
        }
        logger->debug("Map[",i,"]: ",tempKey,", ",tempVal);
    }
    isDone = true;
    logger->debug("Configuration read!");
}

bool Configuration::keyExists(const std::string &key)
{
    return (params->count(key) > 0);
}

std::string Configuration::getString(const std::string &key)
{
    if (isDone)
        return params->at(key);

    return "";
}

int Configuration::getInt(const std::string &key)
{
    if (isDone)
        return std::stoi(params->at(key));

    return -1;
}

bool Configuration::getBool(const std::string &key)
{
    if(!isDone)
        return false;

    std::string value = params->at(key);
    if(value == "TRUE" || value == "ENABLE")
        return true;
    else if(value == "FALSE" || value == "DISABLE")
        return false;
    else
        return false;
}

std::vector<std::string> Configuration::split(const std::string& s,char delim)
{
    std::stringstream ss(s);
    std::vector<std::string> temp;
    std::string seg;
    while (std::getline(ss, seg, delim))
    {
        temp.push_back(seg);
    }
    return temp;
}
