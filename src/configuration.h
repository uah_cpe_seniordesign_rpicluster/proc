//
// Created by evan on 10/14/18.
//

#ifndef PROC_CONFIGURATION_H
#define PROC_CONFIGURATION_H

#include <iostream>
#include <fstream>
#include <mutex>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <string>

#include "logger.h"

class Configuration
{
private:
    ~Configuration();
    Configuration();

    static Configuration * instance;
    static std::mutex l_mutex;

    void init();

    void readFile();
    void setDefault();
    std::vector<std::string> split(const std::string& s,char delim);
    bool isDone;
    std::fstream configFile;
    std::map<std::string,std::string> * params;
    std::vector<std::string> fileContents;

    Logger * logger;

public:

    static Configuration * getInstance();
    bool keyExists(const std::string &key);
    std::string getString(const std::string &key);
    int getInt(const std::string &key);
    bool getBool(const std::string &key);
};


#endif //PROC_CONFIGURATION_H
