#include "spi.h"

/**
 * @brief Overloaded constructor. let user set member variables to
 * and then call spiOpen()
 * @param devspi
 * @param spiMode
 * @param spiSpeed
 * @param spibitsPerWord
 * @throws SpiException
 */
Spi::Spi(const std::string &devspi, Mode spiMode, Speed spiSpeed, BitsPerWord spibitsPerWord) noexcept(false)
{
    this->spifd = -1;

    switch(spiMode)
    {
        case Mode::MODE_0: this->mode = Mode::MODE_0; break;
        case Mode::MODE_1: this->mode = Mode::MODE_1; break;
        case Mode::MODE_2: this->mode = Mode::MODE_2; break;
        case Mode::MODE_3: this->mode = Mode::MODE_3; break;
    }

    switch(spiSpeed)
        {
        case Speed::SPEED_100000:   this->speed = Speed::SPEED_100000;   break; //100KHz
        case Speed::SPEED_250000:   this->speed = Speed::SPEED_250000;   break; //250KHz
        case Speed::SPEED_500000:   this->speed = Speed::SPEED_500000;   break; //500KHz
        case Speed::SPEED_1000000:  this->speed = Speed::SPEED_1000000;  break; //1MHz
        case Speed::SPEED_2000000:  this->speed = Speed::SPEED_2000000;  break; //2Mhz
        case Speed::SPEED_5000000:  this->speed = Speed::SPEED_5000000;  break; //5Mhz
        case Speed::SPEED_10000000: this->speed = Speed::SPEED_10000000; break; //10Mhz
    }

    switch(spibitsPerWord)
    {
        case BitsPerWord::BITS_PER_8:   this->bitsPerWord = BitsPerWord::BITS_PER_8;  break;
        case BitsPerWord::BITS_PER_16:  this->bitsPerWord = BitsPerWord::BITS_PER_16; break;
        case BitsPerWord::BITS_PER_32:  this->bitsPerWord = BitsPerWord::BITS_PER_32; break;
        case BitsPerWord::BITS_PER_64:  this->bitsPerWord = BitsPerWord::BITS_PER_64; break;
    }

    this->spiOpen(devspi);
}

/**
 * @brief Destructor: calls spiClose()
 * @throws SpiException
 */
Spi::~Spi() noexcept(false)
{
    this->spiClose();
}

/**
 * @brief spiOpen() :function is called by the constructor.
 * It is responsible for opening the spidev device
 * "devspi" and then setting up the spidev interface.
 * private member variables are used to configure spidev.
 * They must be set appropriately by constructor before calling
 * this function.
 * @param devspi
 * @throws SpiException
 */
void Spi::spiOpen(const std::string &devspi) noexcept(false)
{
    int statusVal = -1;
    this->spifd = open(devspi.c_str(), O_RDWR);
    if(this->spifd < 0) {
        throw SpiException("Could not open SPI device");
    }

    statusVal = ioctl (this->spifd, SPI_IOC_WR_MODE, &(this->mode));
    if(statusVal < 0) {
        throw SpiException("Could not set SPIMode (WR)...ioctl fail");
    }

    statusVal = ioctl (this->spifd, SPI_IOC_RD_MODE, &(this->mode));
    if(statusVal < 0) {
        throw SpiException("Could not set SPIMode (RD)...ioctl fail");
    }

    statusVal = ioctl (this->spifd, SPI_IOC_WR_BITS_PER_WORD, &(this->bitsPerWord));
    if(statusVal < 0) {
        throw SpiException("Could not set SPI bitsPerWord (WR)...ioctl fail");
    }

    statusVal = ioctl (this->spifd, SPI_IOC_RD_BITS_PER_WORD, &(this->bitsPerWord));
    if(statusVal < 0) {
        throw SpiException("Could not set SPI bitsPerWord(RD)...ioctl fail");
    }

    statusVal = ioctl (this->spifd, SPI_IOC_WR_MAX_SPEED_HZ, &(this->speed));
    if(statusVal < 0) {
        throw SpiException("Could not set SPI speed (WR)...ioctl fail");
    }

    statusVal = ioctl (this->spifd, SPI_IOC_RD_MAX_SPEED_HZ, &(this->speed));
    if(statusVal < 0) {
        throw SpiException("Could not set SPI speed (RD)...ioctl fail");
    }
}

/**
 * @brief spiClose(): Responsible for closing the spidev interface.
 * Called in destructor
 * @throws SpiException
 */
void Spi::spiClose() noexcept(false)
{
    if(close(this->spifd) < 0)
        throw SpiException("Could not close SPI device");
}

/**
 * @brief This function writes data "data" of length "length" to the spidev
 * device. Data shifted in from the spidev device is saved back into
 * "data".
 * @param data
 * @param len
 * @throws SpiException
 */
void Spi::spiWriteRead(char* data, unsigned int len) noexcept(false)
{
    struct spi_ioc_transfer packet;
    int retVal = -1;

    // one spi transfer for each byte

    memset(&packet, 0, sizeof(packet));
    packet.tx_buf           = (unsigned long)data; // transmit from "data"
    packet.rx_buf           = (unsigned long)data; // receive into "data"
    packet.len              = len;
    packet.delay_usecs      = 0;
    packet.speed_hz         = this->speed;
    packet.bits_per_word    = this->bitsPerWord;
    packet.cs_change        = 0;

    retVal = ioctl(this->spifd, SPI_IOC_MESSAGE(1), &packet) ;

    if(retVal < 0) {
        throw SpiException("Problem transmitting spi data..ioctl");
    }
}
