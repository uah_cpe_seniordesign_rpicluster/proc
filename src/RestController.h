//
// Created by evan on 12/24/18.
//

#ifndef PROC_RESTCONTROLLER_H
#define PROC_RESTCONTROLLER_H

#include <cpprest/http_client.h>
#include <cpprest/json.h>

#include "JSONBuilder.h"
#include "logger.h"

class RestController
{
public:
    RestController();
    RestController(JSONBuilder * bui, const std::string &host);
    ~RestController();

    //Getters and setters
    void setBaseHostUrl(const std::string &baseHost);
    void setJSONBuilder(JSONBuilder * bui);

    //Data communication methods
    void post(const std::string &uri); //Used to send data to service.

private:
    void init();
    bool status();

    JSONBuilder * builder;
    Logger * logger;
    std::string baseHost;
};


#endif //PROC_RESTCONTROLLER_H
