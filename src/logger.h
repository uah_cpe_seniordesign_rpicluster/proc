//
// Created by evan on 10/12/18.
//

#ifndef PROC_LOGGER_H
#define PROC_LOGGER_H

#include <iostream>
#include <mutex>
#include <chrono>
#include <fstream>
#include <sstream>

enum class LogType
{
    INFO,
    DEBUG,
    TRACE,
    WARNING,
    CRITICAL
};

class Logger
{
private:
    Logger();
    ~Logger();

    static Logger * instance;
    static std::mutex l_mutex;

    std::fstream logFile;

    std::string getTime();
    bool stdout = true;

    bool Trace = false;
    bool Debug = false;
    bool Info = false;

    template <typename T>
    void writeLogToFile(const T &t, const LogType &ty)
    {
        std::lock_guard<std::mutex> lock(l_mutex); //Lock for IO
        if (!logFile.is_open()) return;
        switch (ty)
        {
            case LogType::DEBUG: logFile << "[" << this->getTime() << "] [DEBUG]: " << t << std::endl; break;
            case LogType::TRACE: logFile << "[" << this->getTime() << "] [TRACE]: " << t << std::endl; break;
            case LogType::WARNING: logFile << "[" << this->getTime() << "] [WARNING]: " << t << std::endl; break;
            case LogType::INFO: logFile << "[" << this->getTime() << "] [INFO]: " << t << std::endl; break;
            case LogType::CRITICAL: logFile << "[" << this->getTime() << "] [CRITICAL!]: " << t << std::endl; break;
            default: break;
        }
    }
    template <typename T>
    void writeToStdout(const T &t, const LogType &ty)
    {
        std::lock_guard<std::mutex> lock(l_mutex); //Lock for IO
        switch (ty)
        {
            case LogType::DEBUG: std::cout << "[" << this->getTime() << "] [DEBUG]: " << t << std::endl; break;
            case LogType::TRACE: std::cout << "[" << this->getTime() << "] [TRACE]: " << t << std::endl; break;
            case LogType::WARNING: std::cout << "[" << this->getTime() << "] [WARNING]: " << t << std::endl; break;
            case LogType::INFO: std::cout << "[" << this->getTime() << "] [INFO]: " << t << std::endl; break;
            case LogType::CRITICAL: std::cout << "[" << this->getTime() << "] [CRITICAL!]: " << t << std::endl; break;
            default: break;
        }
    }
public:
    static Logger * getInstance();
    void setStdoutOutput(bool b);
    void setLoggingLevel(LogType ty);

    template <typename ... T>
    void trace(const T& ...t)
    {
        if (!this->Trace)
            return;

        std::stringstream ss;
        std::initializer_list<int> { ( ss << t, 0 )... };
        this->writeLogToFile(ss.str(),LogType::TRACE);
        if (this->stdout) { this->writeToStdout(ss.str(),LogType::TRACE); }
    }
    template <typename ... T>
    void debug(const T& ... t)
    {
        if (!this->Debug)
            return;

        std::stringstream ss;
        std::initializer_list<int> { (ss << t, 0)... };
        this->writeLogToFile(ss.str(),LogType::DEBUG);
        if (this->stdout) { this->writeToStdout(ss.str(),LogType::DEBUG); }
    }
    template <typename ... T>
    void info(const T& ... t)
    {
        if (!this->Info)
            return;

        std::stringstream ss;
        std::initializer_list<int> { ( ss << t, 0 )... };
        this->writeLogToFile(ss.str(),LogType::INFO);
        if (this->stdout) { this->writeToStdout(ss.str(),LogType::INFO); }
    }
    template <typename ... T>
    void warning(const T& ... t)
    {
        std::stringstream ss;
        std::initializer_list<int> { ( ss << t,0 )... };
        this->writeLogToFile(ss.str(),LogType::WARNING);
        if (this->stdout) { this->writeToStdout(ss.str(),LogType::WARNING); }
    }

    template <typename ... T>
    void critical(const T& ... t)
    {
        std::stringstream ss;
        std::initializer_list<int> { ( ss << t, 0 )... };
        this->writeLogToFile(ss.str(),LogType::CRITICAL);
        if (this->stdout) { this->writeToStdout(ss.str(),LogType::CRITICAL); }
    }
};


#endif //PROC_LOGGER_H
