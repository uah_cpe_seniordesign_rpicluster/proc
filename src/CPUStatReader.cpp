//
// Created by evan on 1/23/19.
//

#include "CPUStatReader.h"

CPUStatReader::CPUStatReader()
{
    logger = Logger::getInstance();
}
CPUStatReader::~CPUStatReader()
{
    logger->debug("Destruction of CPUStatReader().");
    if (this->stat.is_open())
        this->stat.close();
}

std::vector<std::vector<double>> CPUStatReader::getPerCoreCPUUtilization()
{
    stat.open("/proc/stat", std::fstream::in);
    if (!stat.is_open())
    {
        logger->warning("/proc/stat failed to open for initial read.");
        return std::vector<std::vector<double>>();
    }
    std::string temp1;
    std::vector<ProcStat> cpu1;
    while ( std::getline( stat,temp1 ) )
    {
        if (!temp1.compare(0,3,"cpu"))
        {
            std::istringstream ss(temp1);
            cpu1.push_back(ProcStat());
            ProcStat &Entry = cpu1.back();

            ss >> Entry.cpu;
            if (Entry.cpu.size() > 3)
            {
                Entry.cpu.erase(0,3);
            }
            else
            {
                Entry.cpu = "tot";
            }

            for (int i = 0; i < 10; i++)
            {
                ss >> Entry.vals[i];
            }
        }
    }

    stat.close();

    std::this_thread::sleep_for( std::chrono::milliseconds(100));
    stat.open("/proc/stat", std::fstream::in);
    if (!stat.is_open())
    {
        logger->warning("/proc/stat failed to open for final reading.");
        return std::vector<std::vector<double>>();
    }
    std::string temp2;
    std::vector<ProcStat> cpu2;
    while ( std::getline( stat,temp2 ) )
    {
        if (!temp2.compare(0,3,"cpu"))
        {
            std::istringstream ss(temp2);
            cpu2.push_back(ProcStat());
            ProcStat &newEntry = cpu2.back();

            ss >> newEntry.cpu;
            if (newEntry.cpu.size() > 3)
            {
                newEntry.cpu.erase(0,3);
            }
            else
            {
                newEntry.cpu = "tot";
            }

            for (int i = 0; i < 10; i++)
            {
                ss >> newEntry.vals[i];
            }
        }
    }
    stat.close();
    const size_t NUM_SIZE = cpu1.size();

    std::vector<std::vector<double>> tempVector;

    for (size_t i = 0; i < NUM_SIZE; i++)
    {
        const ProcStat & e1 = cpu1[i];
        const ProcStat & e2 = cpu2[i];
        const double active = static_cast<double>( this->getActiveTime(e2) - this->getActiveTime(e1) );
        const double idle = static_cast<double>( this->getIdleTime(e2) - this->getIdleTime(e1) );
        const double total = active + idle;
        tempVector.push_back(std::vector<double> { (idle/total)*100.0,(active/total)*100.0 });
    }
    return tempVector;
}

size_t CPUStatReader::getActiveTime(const ProcStat &c)
{
    return c.vals[USER] +
           c.vals[NICE] +
           c.vals[SYSTEM] +
           c.vals[IRQ] +
           c.vals[SOFTIRQ] +
           c.vals[STEAL] +
           c.vals[GUEST] +
           c.vals[GUEST_NICE];
}

size_t CPUStatReader::getIdleTime(const ProcStat &c)
{
    return c.vals[IOWAIT] + c.vals[IDLE];
}
