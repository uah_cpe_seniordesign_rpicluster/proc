#include "hoststatreader.h"

HostStatReader::HostStatReader()
{
    this->init();

}

HostStatReader::~HostStatReader()
{
    logger->debug("Destruction of HostStatReader()");
    delete memoryStatReader;
    delete cpuStatReader;
    delete thermalStatReader;
    delete netStatReader;
}

StatContainer * HostStatReader::getData()
{
    this->readAll();
    return &cont;
}

void HostStatReader::init()
{
    logger = Logger::getInstance();
    memoryStatReader = new MemoryStatReader();
    cpuStatReader = new CPUStatReader();
}

void HostStatReader::readAll()
{
    Configuration* config = Configuration::getInstance();

    this->readTotalVMem();
    this->readVMemUsed();
    this->readTotalPMem();
    this->readPMemUsed();
    this->readCpuPercentage();
    this->readHostName();
    this->readTemperature();
    this->readRxBytes();
    this->readRxPackets();
    this->readTxBytes();
    this->readTxPackets();

    if(config->getBool("ADC_READING"))
        this->readADC();
}

void HostStatReader::readRxBytes()
{
    this->cont.rx_bytes = netStatReader->getRxBytes();
}

void HostStatReader::readRxPackets()
{
    this->cont.rx_packets = netStatReader->getRxPackets();
}

void HostStatReader::readTxBytes()
{
    this->cont.tx_bytes = netStatReader->getTxBytes();
}

void HostStatReader::readTxPackets()
{
    this->cont.tx_packets = netStatReader->getTxPackets();
}

void HostStatReader::readTotalVMem()
{
    cont.vMem = memoryStatReader->getTotalVMem();
}

void HostStatReader::readVMemUsed()
{
    cont.vMemUsed = memoryStatReader->getTotalVMemUsed();
}

void HostStatReader::readTotalPMem()
{
    cont.pMem = memoryStatReader->getTotalPMem();
}

void HostStatReader::readPMemUsed()
{
    cont.pMemUsed = memoryStatReader->getTotalPMemUsed();
}

void HostStatReader::readCpuPercentage()
{
    cont.perCoreCpuPercentage = cpuStatReader->getPerCoreCPUUtilization();
}

void HostStatReader::readHostName()
{
    char hostname[1023];
    gethostname(hostname,1023);
    std::string temp(hostname);
    cont.hostname = temp;
}

void HostStatReader::readTemperature()
{
    cont.temperature = this->thermalStatReader->getTemperature();
}

void HostStatReader::readADC()
{
    std::vector<std::tuple<double, std::string>> temp = mcpReader->getAllChannels();
    cont.ambientTemperature = std::get<0>(temp.at(0)); //Throwing away the unit of measurement I guess
    cont.moduleCurrentDraw = std::get<0>(temp.at(1));
    cont.backplaneVoltage = std::get<0>(temp.at(2));
    cont.processorVoltage = std::get<0>(temp.at(3));
}

void HostStatReader::setThermalZone(const std::string &thermPath)
{
    this->thermalZonePath = thermPath;
    thermalStatReader = new ThermalStatReader(this->thermalZonePath);
}

void HostStatReader::setNetStatsPath(const std::string &statsPath)
{
    this->netStatsPath = statsPath;
    netStatReader = new NetStatReader(this->netStatsPath);
}

void HostStatReader::setMCPDevSpiPath(const std::string &devspiPath)
{
    this->devSpiPath = devspiPath;
    mcpReader = std::make_unique<MCPReader>(this->devSpiPath);
}
