#ifndef SPI_H
#define SPI_H

//Exceptions
#include "spiexception.h"

//C++ libraries
#include <iostream>
#include <mutex>
#include <string>
#include <vector>

//C libraries
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

class Spi
{
public:
    enum Mode
    {
        MODE_0 = SPI_MODE_0,
        MODE_1 = SPI_MODE_1,
        MODE_2 = SPI_MODE_2,
        MODE_3 = SPI_MODE_3
    };

    enum BitsPerWord
    {
        BITS_PER_8 = 8,
        BITS_PER_16 = 16,
        BITS_PER_32 = 32,
        BITS_PER_64 = 64
    };

    enum Speed
    {
        SPEED_100000 = 100000,
        SPEED_250000 = 250000,
        SPEED_500000 = 500000,
        SPEED_1000000 = 1000000,
        SPEED_2000000 = 2000000,
        SPEED_5000000 = 5000000,
        SPEED_10000000 = 10000000
    };

    Spi(const std::string &devspi, Mode spiMode, Speed spiSpeed, BitsPerWord spibitsPerWord) noexcept(false);
    ~Spi() noexcept(false);

    void spiOpen(const std::string &devspi) noexcept(false);
    void spiClose() noexcept(false);
    void spiWriteRead(char *data, unsigned int len) noexcept(false);

private:
    unsigned char mode;
    unsigned char bitsPerWord;
    unsigned int speed;
    int spifd;
};

#endif // SPI_H
