//
// Created by evan on 1/23/19.
//

#ifndef PROC_CPUSTATREADER_H
#define PROC_CPUSTATREADER_H

#include <thread>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

#include "logger.h"

enum Cats
{
    USER,
    NICE,
    SYSTEM,
    IDLE,
    IOWAIT,
    IRQ,
    SOFTIRQ,
    STEAL,
    GUEST,
    GUEST_NICE
};

typedef struct ProcStat
{
    std::string cpu;
    size_t vals[10];
} ProcStat;

class CPUStatReader
{
private:
    std::fstream stat;
    Logger * logger;
    size_t getActiveTime(const ProcStat &c);
    size_t getIdleTime(const ProcStat &c);
public:
    CPUStatReader();
    ~CPUStatReader();

    std::vector<std::vector<double>> getPerCoreCPUUtilization();
};


#endif //PROC_CPUSTATREADER_H
