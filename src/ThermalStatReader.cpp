//
// Created by evan on 1/23/19.
//

#include "ThermalStatReader.h"

ThermalStatReader::ThermalStatReader(const std::string &file)
{
    logger = Logger::getInstance();
    this->filePath = file;
    zone.open(this->filePath, std::fstream::in);
}

ThermalStatReader::~ThermalStatReader()
{
    logger->debug("Destruction of ThermalStatReader");
    if (this->zone.is_open())
        this->zone.close();
}

/**
 * @brief ThermalStatReader::getTemperature reads the temperature specified from file that the object was instantiated with. Returns the temperature in Celcius.
 * @return
 */
double ThermalStatReader::getTemperature()
{
    if (!this->zone.is_open())
    {
        logger->warning("The file '", this->filePath, "' was unable to open. Cannot read...");
        return 0.0;
    }
    std::string temp;
    std::getline(zone, temp);
    logger->debug("Temperature: ", temp, " (C)");
    double oof = stod(temp);

    return (oof / 1000.0);
}
