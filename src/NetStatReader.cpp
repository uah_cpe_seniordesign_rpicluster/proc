//
// Created by evan on 2/3/19.
//

#include "NetStatReader.h"

NetStatReader::NetStatReader(const std::string &stats)
{
    this->logger = Logger::getInstance();
    this->statsPath = stats; //save stats directory
    this->init();
}

NetStatReader::~NetStatReader()
{
    logger->debug("Destuction of NetStatReader().");
    if (this->tx_packets.is_open())
        this->tx_packets.close();
    if (this->rx_packets.is_open())
        this->rx_packets.close();
    if (this->tx_bytes.is_open())
        this->tx_bytes.close();
    if (this->rx_bytes.is_open())
        this->rx_bytes.close();
}

void NetStatReader::init()
{
    this->tx_packets_val = 0;
    this->rx_packets_val = 0;
    this->tx_bytes_val = 0;
    this->rx_bytes_val = 0;

    this->readPacketsPerSecond();
    this->readBandwidthPerSecond();
}

void NetStatReader::readPacketsPerSecond()
{
    logger->trace("Reading packets/s.");
    std::string rx_packets_file_str = this->statsPath + "/rx_packets";
    std::string tx_packets_file_str = this->statsPath + "/tx_packets";
    this->rx_packets.open(rx_packets_file_str, std::fstream::in);
    if (!this->rx_packets.is_open())
    {
        logger->warning("File: ",rx_packets_file_str,
                ", was unable to open. Cannot read...");
        return;
    }
    this->tx_packets.open(tx_packets_file_str, std::fstream::in);
    if (!this->tx_packets.is_open())
    {
        logger->warning("File: ",tx_packets_file_str,
                        ", was unable to open. Cannot read...");
        return;
    }
    std::string r1_str;
    std::string t1_str;
    std::getline(this->rx_packets, r1_str);
    std::getline(this->tx_packets, t1_str);

    this->rx_packets.close();
    this->tx_packets.close();

    long r1 = atol(r1_str.c_str());
    long t1 = atol(t1_str.c_str());
    logger->trace("r1: ",r1,", t1: ", t1);
    std::this_thread::sleep_for( std::chrono::milliseconds(1000) );

    this->rx_packets.open(rx_packets_file_str, std::fstream::in);
    if (!this->rx_packets.is_open())
    {
        logger->warning("File: ",rx_packets_file_str,
                        ", was unable to open. Cannot read...");
        return;
    }
    this->tx_packets.open(tx_packets_file_str, std::fstream::in);
    if (!this->tx_packets.is_open())
    {
        logger->warning("File: ",tx_packets_file_str,
                        ", was unable to open. Cannot read...");
        return;
    }
    std::string r2_str;
    std::string t2_str;
    std::getline(this->rx_packets, r2_str);
    std::getline(this->tx_packets, t2_str);

    this->rx_packets.close();
    this->tx_packets.close();

    long r2 = atol(r2_str.c_str());
    long t2 = atol(t2_str.c_str());
    logger->trace("r2: ",r2,", t2: ", t2);
    this->rx_packets_val = (r2 - r1);
    this->tx_packets_val = (t2 - t1);
    logger->debug("Received Packets/s: ", this->rx_packets_val, ", Transmitted Packets/s: ",
            this->tx_packets_val);
}

void NetStatReader::readBandwidthPerSecond()
{
    logger->trace("Reading bytes/s.");
    std::string rx_bytes_file_str = this->statsPath + "/rx_bytes";
    std::string tx_bytes_file_str = this->statsPath + "/tx_bytes";

    this->rx_bytes.open(rx_bytes_file_str, std::fstream::in);
    if (!this->rx_bytes.is_open())
    {
        logger->warning("File: ",rx_bytes_file_str,
                        ", was unable to open. Cannot read...");
        return;
    }
    this->tx_bytes.open(tx_bytes_file_str, std::fstream::in);
    if (!this->tx_bytes.is_open())
    {
        logger->warning("File: ",tx_bytes_file_str,
                        ", was unable to open. Cannot read...");
        return;
    }
    std::string r1_str;
    std::string t1_str;
    std::getline(this->rx_bytes, r1_str);
    std::getline(this->tx_bytes, t1_str);



    long r1 = atol(r1_str.c_str());
    long t1 = atol(t1_str.c_str());

    this->rx_bytes.close();
    this->tx_bytes.close();
    this->rx_bytes.clear();
    this->tx_bytes.clear();

    logger->trace("r1: ",r1,", t1: ", t1);
    std::this_thread::sleep_for( std::chrono::milliseconds(1000) );

    this->rx_bytes.open(rx_bytes_file_str, std::fstream::in);
    if (!this->rx_bytes.is_open())
    {
        logger->warning("File: ",rx_bytes_file_str,
                        ", was unable to open. Cannot read...");
        return;
    }
    this->tx_bytes.open(tx_bytes_file_str, std::fstream::in);
    if (!this->tx_bytes.is_open())
    {
        logger->warning("File: ",tx_bytes_file_str,
                        ", was unable to open. Cannot read...");
        return;
    }
    std::string r2_str;
    std::string t2_str;
    std::getline(this->rx_bytes, r2_str);
    std::getline(this->tx_bytes, t2_str);

    this->rx_bytes.close();
    this->tx_bytes.close();
    this->rx_bytes.clear();
    this->tx_bytes.clear();

    long r2 = atol(r2_str.c_str());
    long t2 = atol(t2_str.c_str());
    logger->trace("r2: ",r2,", t2: ", t2);

    this->rx_bytes_val = static_cast<double>((r2 - r1)) / 1024;
    this->tx_bytes_val = static_cast<double>((t2 - t1)) / 1024;
    logger->debug("Received kB/s: ", this->rx_bytes_val, ", Transmitted kB/s: ",
                  this->tx_bytes_val);
}

int NetStatReader::getRxPackets()
{
    return this->rx_packets_val;
}

double NetStatReader::getRxBytes()
{
    return this->rx_bytes_val;
}

int NetStatReader::getTxPackets()
{
    return this->tx_packets_val;
}

double NetStatReader::getTxBytes()
{
    return this->tx_bytes_val;
}




