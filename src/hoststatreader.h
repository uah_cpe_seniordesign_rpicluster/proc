#include <sys/types.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <unistd.h>
#include <memory>

#include "logger.h"
#include "StatContainer.h"
#include "MemoryStatReader.h"
#include "CPUStatReader.h"
#include "ThermalStatReader.h"
#include "NetStatReader.h"
#include "mcpreader.h"
#include "configuration.h"

class HostStatReader
{
public:
    HostStatReader();
    ~HostStatReader();
    StatContainer * getData();

    void setThermalZone(const std::string &thermPath);
    void setNetStatsPath(const std::string &statsPath);
    void setMCPDevSpiPath(const std::string &devspiPath);

private:
    Logger * logger;
    struct StatContainer cont;

    MemoryStatReader * memoryStatReader;
    CPUStatReader * cpuStatReader;
    ThermalStatReader * thermalStatReader;
    NetStatReader * netStatReader;
    std::unique_ptr<MCPReader> mcpReader;

    std::string thermalZonePath;
    std::string netStatsPath;
    std::string devSpiPath;

    void init();
    void readAll();
    void readTotalVMem();
    void readVMemUsed();
    void readTotalPMem();
    void readPMemUsed();
    void readHostName();
    void readCpuPercentage();
    void readTemperature();
    void readRxBytes();
    void readRxPackets();
    void readTxBytes();
    void readTxPackets();
    void readADC();
};
