#include "MCP320X.h"

/**
 * @brief MCP320X::MCP320X constructor for a MCP3204 device with 4 channels. Params are for each channel and what type of measurement is being taken.
 * @param ch0
 * @param ch1
 * @param ch2
 * @param ch3
 */
MCP320X::MCP320X(const std::string &devspi, ChannelType ch0, ChannelType ch1, ChannelType ch2, ChannelType ch3)
{
    this->logger = Logger::getInstance();
    logger->debug("Constructing an MCP320X class");

    spi = new Spi(devspi, Spi::MODE_0, Spi::SPEED_1000000, Spi::BITS_PER_8); //Assuming device is on spidev0.0. Very well could not be... but it is...
    this->type = MCPType::MCP3204;
    this->numChannels = 4;

    this->channelTypes.at(0) = ch0;
    this->channelTypes.at(1) = ch1;
    this->channelTypes.at(2) = ch2;
    this->channelTypes.at(3) = ch3;

    this->channelTypes.at(4) = ChannelType::Unused;
    this->channelTypes.at(5) = ChannelType::Unused;
    this->channelTypes.at(6) = ChannelType::Unused;
    this->channelTypes.at(7) = ChannelType::Unused;
}

/**
 * @brief MCP320X::MCP320X constructor for a MCP3208 device with 8 channels. Params are for each channel and what type of measurement is being taken.
 * @param ch0
 * @param ch1
 * @param ch2
 * @param ch3
 * @param ch4
 * @param ch5
 * @param ch6
 * @param ch7
 */
MCP320X::MCP320X(const std::string &devspi, ChannelType ch0, ChannelType ch1, ChannelType ch2, ChannelType ch3, ChannelType ch4, ChannelType ch5, ChannelType ch6, ChannelType ch7)
{
    this->logger = Logger::getInstance();
    logger->debug("Constructing an MCP320X class");

    spi = new Spi(devspi, Spi::MODE_0, Spi::SPEED_1000000, Spi::BITS_PER_8); //Assuming device is on spidev0.0. Very well could not be... but it is...
    this->type = MCPType::MCP3208;
    this->numChannels = 8;

    this->channelTypes.at(0) = ch0;
    this->channelTypes.at(1) = ch1;
    this->channelTypes.at(2) = ch2;
    this->channelTypes.at(3) = ch3;
    this->channelTypes.at(4) = ch4;
    this->channelTypes.at(5) = ch5;
    this->channelTypes.at(6) = ch6;
    this->channelTypes.at(7) = ch7;
}

MCP320X::~MCP320X()
{
    logger->debug("Deconstructing a MCP320X class");
    delete spi;
}

/**
 * @brief MCP320X::getChannel returns a tuple containing channel data and the unit of measurement.
 * @param ch
 * @throws NegativeIntegerException, InputOutOfRangeException
 * @return
 */
std::tuple<double, std::string> MCP320X::getChannel(const int ch) const noexcept(false)
{
    if(ch < 0)
        throw NegativeIntegerException("No negative arguments allowed.");
    if(ch > numChannels-1)
        throw InputOutOfRangeException("Cannot read channel that does not exist.");

    uint16_t adcVal = 0;
    char data[3]; //Data to send and receive
    data[0] = (channelMsgs[ch] >> 21); //Put the data transmission message into the data message
    data[1] = (channelMsgs[ch] >> 13);
    data[2] = (channelMsgs[ch] >> 0);

    spi->spiWriteRead(data, 3);

    adcVal = (((uint16_t)data[1] << 8) | ((uint16_t)data[2])); //Puts the bytes back together into a 16 bit number
    adcVal &= 0x0FFF; //Ensure without a doubt that only the lower 12 bits are used

    double volts = digitalToVolts(adcVal);

    logger->trace("Calculated voltage of channel " + std::to_string(ch) + " was " + std::to_string(volts) + "V");

    switch(this->channelTypes.at(ch))
    {
        case ChannelType::Voltage: return std::tuple<double, std::string>(volts, "V");
        case ChannelType::Current: return std::tuple<double, std::string>(toCurrent(volts), "mA");
        case ChannelType::Temperature: return std::tuple<double, std::string>(toTemperature(volts), "C");
        default: return std::tuple<double, std::string>(0.0, "None");
    }
}

/**
 * @brief MCP320X::getAllChannels returns a vector of tuples containing all channel data and units of measurement.
 * @return
 */
std::vector<std::tuple<double, std::string>> MCP320X::getAllChannels() const
{
    std::vector<std::tuple<double, std::string>> data;

    for(int i = 0; i < this->numChannels; i++)
        data.push_back(this->getChannel(i));

    return data;
}

/**
 * @brief MCP320X::getType returns the current MCP type that is configured.
 * @return
 */
MCP320X::MCPType MCP320X::getType() const
{
    return this->type;
}

double MCP320X::digitalToVolts(const uint16_t digital) const //Returns in V
{
    return double(digital)*0.00080566; //This is how many volts the LSB of a 12bit ADC is
}

double MCP320X::toTemperature(const double volts) const //Returns in C
{
    //From the thermistor datasheet plus some Excel spreadsheet calculations...
    return ( (-6.3871*pow(volts,5)) + (55.74*pow(volts,4)) - (188.72*pow(volts,3)) + (310.93*pow(volts,2)) - (285.47*volts) + (162.56) );
}

double MCP320X::toCurrent(const double volts) const //Returns in mA
{
    //Reduces the gain by a factor of 20, then V/R=I, then * 1000 to convert into mA
    return ((volts/20)/0.01)*1000.0;
}
