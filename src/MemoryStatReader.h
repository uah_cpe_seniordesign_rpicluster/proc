//
// Created by evan on 1/23/19.
//

#ifndef PROC_MEMORYSTATREADER_H
#define PROC_MEMORYSTATREADER_H

#include <sys/sysinfo.h>
#include <sys/types.h>

#include "logger.h"

// Class for getting all of the memory stats... ugh
class MemoryStatReader
{
private:
    struct sysinfo memInfo;
    Logger * logger;

public:
    MemoryStatReader();
    ~MemoryStatReader();

    int64_t getTotalVMem();
    int64_t getTotalVMemUsed();

    int64_t getTotalPMem();
    int64_t getTotalPMemUsed();

};


#endif //PROC_MEMORYSTATREADER_H
