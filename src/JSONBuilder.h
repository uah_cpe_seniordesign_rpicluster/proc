//
// Created by evan on 12/24/18.
//

#ifndef PROC_JSONBUILDER_H
#define PROC_JSONBUILDER_H

#include <cpprest/json.h>
#include "StatContainer.h"
#include "logger.h"
#include <string>

using namespace web;

class JSONBuilder
{
public:
    JSONBuilder(StatContainer * cont);
    ~JSONBuilder();

    json::value getJsonObject();
private:
    Logger * logger;
    StatContainer * container;
};


#endif //PROC_JSONBUILDER_H
