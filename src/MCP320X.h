#ifndef MCP320X_H
#define MCP320X_H

//C++ libraries
#include <tuple>
#include <string>
#include <vector>
#include <array>

//C libraries
#include <math.h>

//Our libraries :D
#include "spi.h"
#include "logger.h"
#include "negativeintegerexception.h"
#include "inputoutofrangeexception.h"

/**
 * @brief The MCP320X class provides an object oriented style of reading a MCP3204 or MCP3208 12 bit ADC.
 */
class MCP320X
{
public:
    /**
     * @brief The MCPType enum provides a typed method of determining if a device is either a MCP3204 or an MCP3208.
     */
    enum MCPType
    {
        MCP3204,
        MCP3208
    };

    /**
     * @brief The ChannelType enum provides a typed method of determining if channels are measuring a voltage, current, temperature, or if they're unused.
     */
    enum ChannelType
    {
        Voltage,
        Current,
        Temperature,
        Unused
    };

    MCP320X(const std::string &devspi, ChannelType ch0, ChannelType ch1, ChannelType ch2, ChannelType ch3);
    MCP320X(const std::string &devspi, ChannelType ch0, ChannelType ch1, ChannelType ch2, ChannelType ch3, ChannelType ch4, ChannelType ch5, ChannelType ch6, ChannelType ch7);
    ~MCP320X();
    std::tuple<double, std::string> getChannel(const int ch) const noexcept(false);
    std::vector<std::tuple<double, std::string>> getAllChannels() const;
    MCPType getType() const;


private:
    Logger* logger;
    MCPType type;
    std::array<ChannelType, 8> channelTypes;
    int numChannels;

    /* From MCP320X datasheet:
     * Start SINGLE/DIFF D2 D1 D0
     * 1 1 0 0 0 = 0xC00000
     * 1 1 0 0 1 = 0xC80000
     * 1 1 0 1 0 = 0xD00000
     * 1 1 0 1 1 = 0xD80000
     * 1 1 1 0 0 = 0xE00000
     * 1 1 1 0 1 = 0xE80000
     * 1 1 1 1 0 = 0xF00000
     * 1 1 1 1 1 = 0xF80000
     * */
    const std::array<uint32_t, 8> channelMsgs = {0xC00000, 0xC80000, 0xD00000, 0xD80000, 0xE00000, 0xE80000, 0xF00000, 0xF80000};

    Spi* spi;

    /* Helper functions */
    double digitalToVolts(const uint16_t digital) const;
    double toTemperature(const double volts) const;
    double toCurrent(const double volts) const;

};

#endif // MCP320X_H
