#ifndef INPUTOUTOFRANGEEXCEPTION_H
#define INPUTOUTOFRANGEEXCEPTION_H

#include "exception.h"
#include <string>

class InputOutOfRangeException : public Exception
{
public:
    InputOutOfRangeException(const std::string& msg) : Exception(msg) {}
};

#endif // INPUTOUTOFRANGEEXCEPTION_H
