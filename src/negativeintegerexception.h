#ifndef NEGATIVEINTEGEREXCEPTION_H
#define NEGATIVEINTEGEREXCEPTION_H

#include "exception.h"
#include <string>

class NegativeIntegerException : public Exception
{
public:
    NegativeIntegerException(const std::string& msg) : Exception(msg) {}
};

#endif // NEGATIVEINTEGEREXCEPTION_H
