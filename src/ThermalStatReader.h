//
// Created by evan on 1/23/19.
//

#ifndef PROC_THERMALSTATREADER_H
#define PROC_THERMALSTATREADER_H

#include <string>
#include <fstream>
#include "logger.h"

class ThermalStatReader
{
private:
    std::string filePath;
    std::fstream zone;
    Logger * logger;

public:
    ThermalStatReader(const std::string& file);
    ~ThermalStatReader();

    double getTemperature();

};


#endif //PROC_THERMALSTATREADER_H
