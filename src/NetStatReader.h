//
// Created by evan on 2/3/19.
//

#ifndef PROC_NETSTATREADER_H
#define PROC_NETSTATREADER_H

#include <iostream>
#include <fstream>
#include <thread>
#include <stdlib.h>

#include "logger.h"

class NetStatReader
{
public:
    NetStatReader(const std::string & statsPath);
    ~NetStatReader();

    double getRxBytes();
    int getRxPackets();

    double getTxBytes();
    int getTxPackets();

private:

    std::string statsPath;

    std::fstream rx_packets;
    std::fstream tx_packets;

    double tx_packets_val;
    double rx_packets_val;

    std::fstream rx_bytes;
    std::fstream tx_bytes;

    double tx_bytes_val;
    double rx_bytes_val;

    Logger * logger;

    void readPacketsPerSecond();
    void readBandwidthPerSecond();
    void init();

};


#endif //PROC_NETSTATREADER_H
