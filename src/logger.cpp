//
// Created by evan on 10/12/18.
//

#include "logger.h"

Logger * Logger::instance = nullptr;
std::mutex Logger::l_mutex;

Logger::Logger()
{
    logFile.open("./daemon.log", std::fstream::out);
}
Logger::~Logger()
{
    logFile.close();
}
Logger * Logger::getInstance()
{
    std::lock_guard<std::mutex> lock(l_mutex); //Lock for instance.

    if (instance == nullptr)
        instance = new Logger();

    return instance;
}

std::string Logger::getTime()
{
    std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    return std::to_string(ms.count());
}
void Logger::setStdoutOutput(bool b)
{
    this->stdout = b;
}
void Logger::setLoggingLevel(LogType ty)
{
    switch (ty)
    {
        case LogType::TRACE: this->Trace = true;
        case LogType::DEBUG: this->Debug = true;
        case LogType::INFO : this->Info  = true;
        default: this->Info = true; //default log level
    }
}
