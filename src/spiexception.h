#ifndef SPIEXCEPTION_H
#define SPIEXCEPTION_H

#include "exception.h"
#include <string>

class SpiException : public Exception
{
public:
    SpiException(const std::string& msg) : Exception(msg) {}
};

#endif // SPIEXCEPTION_H
