#ifndef MCPREADER_H
#define MCPREADER_H

#include "logger.h"
#include "MCP320X.h"

class MCPReader
{
private:
    Logger* logger;
    MCP320X* mcp;
    std::string devspiPath;

    void printInfo(const std::tuple<double, std::string> &info) const;
    void printInfo(const std::vector<std::tuple<double, std::string>> &info) const;

public:
    MCPReader(const std::string &devSpiPath);
    ~MCPReader();

    std::tuple<double, std::string> getChannel(const int ch) const;
    std::vector<std::tuple<double, std::string>> getAllChannels() const;
};

#endif // MCPREADER_H
