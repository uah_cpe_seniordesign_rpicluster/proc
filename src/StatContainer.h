//
// Created by evan on 12/24/18.
//

#ifndef PROC_STATCONTAINER_H
#define PROC_STATCONTAINER_H
struct StatContainer
{
    int64_t vMem;
    int64_t vMemUsed;
    int64_t pMem;
    int64_t pMemUsed;
    double temperature;
    int tx_packets;
    int rx_packets;
    double tx_bytes;
    double rx_bytes;
    std::string hostname;
    std::vector<std::vector<double>> perCoreCpuPercentage; //vector in { idle, active } in order of core. elem 0 is total
    double ambientTemperature;
    double moduleCurrentDraw;
    double backplaneVoltage;
    double processorVoltage;
};
#endif //PROC_STATCONTAINER_H
