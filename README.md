# Proc - RPi Process Monitoring Application

### Pre-Build Dependencies

- gcc-g++
- cmake 3.7 (or higher)
- git

### Building on RPM based linux:

- Install the following dependencies via YUM: `openssl-devel boost-devel cpprest-devel` OR in debian `libboost-all-dev libssl-dev libcpprest-dev`
- Clone the project `git clone <GIT_URL>`
- Change directory to the project root and create a `build` directory.
- Change directory to the build directory and run `cmake3 ../`
- On completion, run `make` to build the executable.
- Done! Yay.

### Startup Configuration File Directives:

- `BASE_REST_URL`: This directive specifies the base URL of the dataservice, default value is: `http://127.0.0.1:8081`
- `POST_URI`: This directive specifies the path to the post URL, default value is: `/api/addHostData`
- `ITERATION_DELAY`: This directive specifies the heartbeat interval in seconds, default value is: `5`
- `LOGGING`: This directive specifies the logging level. Value can be any of the following: `[ TRACE | DEBUG | INFO ]`. Default value is `INFO`
- `THERMAL_ZONE_PATH`: This directive specifies the location of the CPU temperature file. Default is `/sys/class/thermal/thermal_zone0/temp` (this is the rpi location).
- `NET_STATS_PATH` This directive specifies the path to a network interface statistics directory. Default is `/sys/class/net/eth0/statistics`